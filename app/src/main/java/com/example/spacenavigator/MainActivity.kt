package com.example.spacenavigator

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.LinearLayout

class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        refresh()
    }

    private fun refresh(){
        val list = findViewById<LinearLayout>(R.id.list_devices)
        list.removeAllViewsInLayout()
        val devices = getBtDevices()

        devices.forEach{
            val device = it

            val btn = Button(this)
            btn.text = device.name
            btn.height = 50
            btn.setOnClickListener {
                val intent = Intent(this, DataSender::class.java).putExtra("address", device.address)
                startActivity(intent)
            }

            list.addView(btn)
        }
    }

    fun onRefreshButtonClick(view: View) {
        refresh()
    }

}